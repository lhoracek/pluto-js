import React, { Component } from 'react';
import './App.css';
import Scene from "./scene/Scene"

class App extends Component {
  render() {
    return (
      <div className="App">
        <Scene />
      </div>
    );
  }
}

export default App;
