import React, { PureComponent } from 'react';
import './Tile.css';
import tile from './tile.svg';
import PropTypes from 'prop-types';

class Tile extends PureComponent {


  preventDefault(e){
    e.preventDefault(); // Let's stop this event.
  }

  onMouseOver(e){

  }

  onMouseOut(e){

  }

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  render() {

    const left = (this.props.x * 256) / 2;
    const top = ((this.props.y * 128) - ((this.props.x % 2) * (128 / 2))) - this.props.elevation;

    const style = {
      position: 'absolute',
      left: left+'px',
      top: top+'px',
      zIndex: this.props.y + (this.props.x % 2 === 0 ? 1 : 0),
    };

    return (
      <img src={tile} className="Tile" alt="tile" style={style} onDragStart={this.preventDefault}/>
    );
  }
}

Tile.propTypes = {
  elevation: PropTypes.number,
  x: PropTypes.number,
  y: PropTypes.number
};

export default Tile;
