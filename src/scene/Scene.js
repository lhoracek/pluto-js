import React, { Component } from 'react';
import clickdrag from 'react-clickdrag';
import './Scene.css';
import Tile from "./tile/Tile"

class Scene extends Component {

  constructor(props) {
    super(props);

    this.state = {
      lastPositionX: 0,
      lastPositionY: 0,
      currentX: 0,
      currentY: 0
    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.dataDrag.isMoving) {
      this.setState({
        currentX: this.state.lastPositionX + nextProps.dataDrag.moveDeltaX,
        currentY: this.state.lastPositionY + nextProps.dataDrag.moveDeltaY
      });
    }
    else {
      this.setState({
        lastPositionX: this.state.currentX,
        lastPositionY: this.state.currentY
      });
    }
  }

  render() {
    const divStyle = {
      transform: 'translate('+this.state.currentX+'px, '+this.state.currentY+'px)',
    };

    var count = 20
    var tiles = [];
    for (var i=0; i < count; i++) {
       for (var j=0; j < count; j++) {
        tiles.push(<Tile x={i} y={j} elevation={0} />);
    }
    }
    return (
      <div className="SceneWrapper">
        <div className="Scene" style={divStyle}>
          {tiles}
        </div>
      </div>
    );
  }
}

var DragScene = clickdrag(Scene, {touch: true});

export default DragScene;
